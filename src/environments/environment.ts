// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyCVF82pgTtR2iwe2-eYlK_te3pnKkQBsMk",
    authDomain: "daily-board-dev.firebaseapp.com",
    databaseURL: "https://daily-board-dev.firebaseio.com",
    projectId: "daily-board-dev",
    storageBucket: "daily-board-dev.appspot.com",
    messagingSenderId: "854793271690"
  }
};
