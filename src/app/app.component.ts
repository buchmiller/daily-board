import { Component } from '@angular/core';
import { AfService } from './af.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Daily Dashboard';

  constructor(public afService: AfService, private router: Router) {
    this.afService.afAuth.authState.subscribe((auth) => {
      if (auth == null) {
        this.router.navigate(['login']);
      }
    });
  }
}
