import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { AngularFireModule } from 'angularfire2';
import { AngularFirestoreModule } from 'angularfire2/firestore';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { environment } from '../environments/environment';

import { AppComponent } from './app.component';
import { AfService } from './af.service';
import { TasksComponent } from './tasks/tasks.component';
import { LoginPageComponent } from './login-page/login-page.component';
import { TaskModalComponent } from './task-modal/task-modal.component';
import { NewBoardModalComponent } from './new-board-modal/new-board-modal.component';


const ROUTES = [
  {
    path: '',
    redirectTo: 'tasks',
    pathMatch: 'full'
  },
  {
    path: 'login',
    component: LoginPageComponent
  },
  {
    path: 'tasks',
    component: TasksComponent
  }
];
@NgModule({
  declarations: [
    AppComponent,
    TasksComponent,
    LoginPageComponent,
    TaskModalComponent,
    NewBoardModalComponent
  ],
  entryComponents: [
    TaskModalComponent,
    NewBoardModalComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    AngularFireModule.initializeApp(environment.firebase, 'daily-board'),
    AngularFirestoreModule,
    AngularFireAuthModule,
    NgbModule.forRoot(),
    RouterModule.forRoot(ROUTES)
  ],
  providers: [AfService],
  bootstrap: [AppComponent]
})
export class AppModule { }
