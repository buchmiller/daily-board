import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { AfService } from '../af.service';
import { TaskModalComponent } from '../task-modal/task-modal.component';
import { NewBoardModalComponent } from '../new-board-modal/new-board-modal.component';

@Component({
  selector: 'app-tasks',
  templateUrl: './tasks.component.html',
  styleUrls: ['./tasks.component.css']
})
export class TasksComponent implements OnInit {
  newTask = '';

  constructor(public afService: AfService, private modalService: NgbModal) { }

  ngOnInit() { }

  createTask() {
    this.afService.createNewTask(this.newTask);
    this.newTask = '';
  }

  toggleTask(task) {
    if (task.completed) {
      this.afService.uncompleteTask(task.$key);
    } else {
      this.afService.completeTask(task.$key);
    }
  }

  deleteTask(task) {
    event.stopPropagation();
    this.afService.deleteTask(task.$key);
  }

  isAuthenticated(): boolean {
    return this.afService.isAuthenticated;
  }

  openNewBoardModal() {
    const modalRef = this.modalService.open(NewBoardModalComponent);
    modalRef.result.then(data => {
      this.afService.createNewBoard(data);
    }).catch(error => {
      // modal closed without saving
    });
  }

  openEditModal(task, event) {
    event.stopPropagation();
    const modalRef = this.modalService.open(TaskModalComponent);
    const taskCopy = Object.assign({}, task);
    modalRef.componentInstance.task = taskCopy;
    modalRef.result.then(data => {
      this.afService.updateTask(task.$key, data);
    }).catch(error => {
      // modal closed without saving
    });
  }
}
