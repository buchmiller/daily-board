import { Component, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-new-board-modal',
  templateUrl: './new-board-modal.component.html',
  styleUrls: ['./new-board-modal.component.css']
})
export class NewBoardModalComponent implements OnInit {
  boardName = '';

  constructor(public activeModal: NgbActiveModal) { }

  save() {
    this.activeModal.close(this.boardName);
  }

  close() {
    this.activeModal.dismiss('Closed modal');
  }

  ngOnInit() {
  }

}
