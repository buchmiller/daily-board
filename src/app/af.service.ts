import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFirestore, AngularFirestoreCollection } from 'angularfire2/firestore';
import * as firebase from 'firebase/app';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/first';
import { User } from './user';

@Injectable()
export class AfService {
  public isAuthenticated: boolean;
  public user = new User();
  currentBoardKey: string;
  boardsRef: AngularFirestoreCollection<any>;
  boards: Observable<any[]>;
  tasksRef: AngularFirestoreCollection<any>;
  tasks: Observable<any[]>;

  constructor(public afAuth: AngularFireAuth, private router: Router, private db: AngularFirestore) {
    this.afAuth.authState.subscribe((auth) => {
      if (auth == null) {
        this.isAuthenticated = false;
      } else {
        this.isAuthenticated = true;
        // any other user information can also be set here
        this.user.displayName = auth.displayName;
        this.user.id = auth.uid;
        this.db.doc('users/' + auth.uid)
          .set({ name: auth.displayName, email: auth.email }, { merge: true }) // Create user document if not exists
          .then(() => {
            this.boardsRef = this.db.collection('users/' + auth.uid + '/boards');
            this.boards = this.boardsRef.snapshotChanges().map(changes => {
              return changes.map(c => ({ $key: c.payload.doc.id, ...c.payload.doc.data() }));
            });
            this.boards.first().subscribe((boards) => {
              if (boards.length) {
                this.setActiveBoard(boards[0]);
              }
              // else create board modal?
            });
          });
      }
    });
  }

  login() {
    return this.afAuth.auth.signInAnonymously();
  }

  loginGoogle() {
    return this.afAuth.auth.signInWithPopup(new firebase.auth.GoogleAuthProvider()).then(result => {
      const token = result.credential.accessToken;
      const user = result.user;
      return true;
    }).catch(error => {
      console.log('Error logging in with Google.\n' + error);
      return false;
    });
  }

  logout() {
    this.currentBoardKey = null;
    this.boardsRef = null;
    this.boards = null;
    this.tasksRef = null;
    this.tasks = null;
    return this.afAuth.auth.signOut().then(() => {
      return this.router.navigate(['login']);
    });
  }

  setActiveBoard(board: { $key: string, name: string, path: any }) {
    const newPath = board.path.path + '/tasks';
    if (this.tasksRef && newPath === this.tasksRef.ref.path) {
      return;
    }
    this.currentBoardKey = board.$key;
    this.tasksRef = this.db.collection(newPath, ref => ref.orderBy('timestamp').limit(50));
    this.tasks = this.tasksRef.snapshotChanges().map(changes => {
      return changes.map(c => ({ $key: c.payload.doc.id, ...c.payload.doc.data() }));
    });
  }

  createNewBoard(name: string) {
    const newBoard = {
      name: name
    };
    this.db.collection('boards').add(newBoard).then(newBoardRef => {
      const userBoard = {
        name: name,
        path: newBoardRef
      };
      this.boardsRef.add(userBoard);
    });
  }

  createNewTask(text: string) {
    const message = {
      text: text,
      timestamp: 0 - Date.now()
    };
    this.tasksRef.ref.add(message);
  }

  completeTask(key: string) {
    this.tasksRef.doc(key).update({ completed: true });
  }

  uncompleteTask(key: string) {
    this.tasksRef.doc(key).update({ completed: false });
  }

  deleteTask(key: string) {
    this.tasksRef.doc(key).delete();
  }

  updateTask(key: string, task) {
    this.tasksRef.doc(key).update({ text: task.text, completed: task.completed });
  }
}
