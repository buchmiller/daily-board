import { Component, Input } from '@angular/core';

import { NgbModal, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-task-modal',
  templateUrl: './task-modal.component.html',
  styleUrls: ['./task-modal.component.css']
})
export class TaskModalComponent {
  @Input() task;

  constructor(public activeModal: NgbActiveModal) { }

  save() {
    this.activeModal.close(this.task);
  }

  close() {
    this.activeModal.dismiss('Closed modal');
  }

  toggleCompleted() {
    this.task.completed = !this.task.completed;
  }
}
