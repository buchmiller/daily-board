import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AfService } from '../af.service';

@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.css']
})
export class LoginPageComponent implements OnInit {

  constructor(private afService: AfService, private router: Router) {
  }

  ngOnInit() {
    this.afService.afAuth.authState.subscribe((auth) => {
      if (auth != null) {
        this.redirectToHomePage();
      }
    });
  }

  loginAnon() {
    this.afService.login().then(() => {
      this.redirectToHomePage();
    });
  }

  loginGoogle() {
    this.afService.loginGoogle().then((result) => {
      if (result === true) {
        this.redirectToHomePage();
      }
    });
  }

  private redirectToHomePage() {
    this.router.navigate(['']);
  }
}
