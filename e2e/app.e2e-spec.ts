import { DailyBoardPage } from './app.po';

describe('daily-board App', () => {
  let page: DailyBoardPage;

  beforeEach(() => {
    page = new DailyBoardPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!');
  });
});
